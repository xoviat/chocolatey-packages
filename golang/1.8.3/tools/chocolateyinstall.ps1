
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.3.windows-amd64.msi'
	checksum      = 'c1df97e6f9afc2db1705f0f1d04fa31804d5c9b995cca26680881baaa1543eb4'
	checksumType  = 'sha256'
	checksum64    = '35734d9331fe4ee19725213d8e98840660460fd1b90518fffc79c0f856a3586a'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

