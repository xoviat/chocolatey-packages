
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13rc1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13rc1.windows-amd64.msi'
	checksum      = '42d2e04e3d6545b534e3d93d75db38583a48174cfc07568ee84daf84efc8d06e'
	checksumType  = 'sha256'
	checksum64    = '92a44b10ff42ce6114118c6be24559f8179a524519f6c36aff9f5a5855d046ce'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

