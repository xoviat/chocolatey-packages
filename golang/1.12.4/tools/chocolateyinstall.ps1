
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.4.windows-amd64.msi'
	checksum      = '71b08a2a2258d67499cc9f88987c328a8bc8cbea2268ba5fca2aa99b9eb42d25'
	checksumType  = 'sha256'
	checksum64    = 'abf0af385ea9963e9c53d2c46726af7e8aea6c00556817f99fca318e05d177ff'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

