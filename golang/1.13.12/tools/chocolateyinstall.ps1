
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.12.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.12.windows-amd64.msi'
	checksum      = '9e2591fccb6feaccb2a4f5e3746037c6bcf885fa2ee7e3b569ce288782771cff'
	checksumType  = 'sha256'
	checksum64    = '8372a5a8813ff9bad6e5a74390bb270ff864b6fd2ee5e3b4a0401f92a0e3465f'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs
