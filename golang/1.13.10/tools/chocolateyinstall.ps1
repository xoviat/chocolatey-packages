
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.10.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.10.windows-amd64.msi'
	checksum      = '32cb0e51f3e19563655dbd89c7ce46fe318f62c1ab2356e3b05f196a39e0e2d8'
	checksumType  = 'sha256'
	checksum64    = '8e9185998d8e5b6e98c2cf785b85c9891910020cd3828aa81363bae7c89161ad'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs
