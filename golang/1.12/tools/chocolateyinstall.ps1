
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.windows-amd64.msi'
	checksum      = '089a34c9bc0872c66a77e9f6b41d1c07c3841567199ed53bab9bf49a730670bd'
	checksumType  = 'sha256'
	checksum64    = '13515401fc5e8e2959ee37413f9d2af934e2bfbba87a2a4d7a1c6058a9dbe6d5'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

