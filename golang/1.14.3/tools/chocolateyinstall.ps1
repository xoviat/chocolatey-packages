
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.14.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.14.3.windows-amd64.msi'
	checksum      = 'b2bd9cb8d05f2c7279c26b6e0ee4867ad20d2ba386ee37eeae1cfb0ce7745e61'
	checksumType  = 'sha256'
	checksum64    = 'c4f0be14d69f85a2bf4d4fbfd9c879c2f8fcb4560f117df0e17aa1999f8a319e'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs
