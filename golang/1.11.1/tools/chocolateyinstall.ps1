
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.1.windows-amd64.msi'
	checksum      = 'a1933196bc2b87fc20f2ab26e5a379a6b6fd83c7b415216c00ef3a3dd75babb1'
	checksumType  = 'sha256'
	checksum64    = '74ed33958f65f8f294c3bf4dc80cd912e1e8c4820bb35f42fc75059a2e37e493'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

