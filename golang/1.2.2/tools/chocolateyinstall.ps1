
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.2.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.2.2.windows-amd64.msi'
	checksum      = '9da59bee409015fdd7b9290c1e25ad15696a698154bbd3e3fa8ece4e955ef474'
	checksumType  = 'sha256'
	checksum64    = '831da2a4dbf577d43e3589e5fcd3db3568523a5f2c0811030be599b757727129'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

