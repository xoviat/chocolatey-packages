
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.8.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.8.windows-amd64.msi'
	checksum      = '6dd6078c7e0e950a8ab4e4efd02072f83ae165f5a98319988ec3ef75ab9cab85'
	checksumType  = 'sha256'
	checksum64    = 'e31ee61f7df18e45b1ab304536c96f9bd98298891bc09c8a1316dc6747bf7adc'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

