
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.6.windows-amd64.msi'
	checksum      = 'b77a27f81eaed909d7489d01b4b8d6f18790460bcf2c08a8e47a0e0d4bc72fbd'
	checksumType  = 'sha256'
	checksum64    = 'd984e25667e4816978c3d0ad349f621cf2e32cde812bee33c002a461f6e9b194'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

