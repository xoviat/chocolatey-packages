
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.8.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.8.windows-amd64.msi'
	checksum      = 'c1ac3e956a6092af61826630067781664b87eac68fbdffcb090f7f5f1d25110b'
	checksumType  = 'sha256'
	checksum64    = '999af2778b094dc6170466fc867ec0c4be7649f4cc6c28f634308b897fe3b12b'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

