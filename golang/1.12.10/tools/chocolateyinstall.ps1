
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.10.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.10.windows-amd64.msi'
	checksum      = '4665031c107b53349ed5596de99527632f4bebcdc597d2afa3173fdb7eeda33c'
	checksumType  = 'sha256'
	checksum64    = '0e25d194c0be9755afac4ac7f7c20f800825b1c5d77932fbb5017551e9b700a4'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

