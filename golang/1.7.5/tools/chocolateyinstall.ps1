
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.7.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.7.5.windows-amd64.msi'
	checksum      = '1ff1ce677c7a6d18ba88e0518e7fa2167943f2f3c2d56173666f6ef56d52d0e0'
	checksumType  = 'sha256'
	checksum64    = '23064bb5bab2f8a96322b7630c6c8cfbd6fa81ef3350977227df328a76e8e861'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

