
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.6.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.6.1.windows-amd64.msi'
	checksum      = 'aebc1947de9e51ff277c3b227be19d56141dd94d093f3384fb79a001a64fa6da'
	checksumType  = 'sha256'
	checksum64    = '345322f554822176337f9fbf86fa42b6e0430412fea24cd814182f0a394687a3'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

