
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.6.windows-amd64.msi'
	checksum      = '4b97ed40d6b3a687c65d53027153bd042198bcf09d65dd80b57dc2805615f26c'
	checksumType  = 'sha256'
	checksum64    = 'ee3157e83e94b39f234bcb5293b5f285c52a19eeae7cc7aedc8c3ffe0ea66a6a'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

