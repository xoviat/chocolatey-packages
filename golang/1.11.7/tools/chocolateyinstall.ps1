
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.7.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.7.windows-amd64.msi'
	checksum      = '2bf7529b6a8ed08dad02d1e6e515e4602d93640827119844eb3a3c115306967b'
	checksumType  = 'sha256'
	checksum64    = 'a592cbb3a794c86a730d9d48980f8e0d18b65ee320d1150228007b6df2167882'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

