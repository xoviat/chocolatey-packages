
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.3.windows-amd64.msi'
	checksum      = '2a395c4a26a9d682c0db8842d9fc64226ab398f087401b574d688f4dfdc3f270'
	checksumType  = 'sha256'
	checksum64    = 'cb8975b55340cd9c9cd120c49bb18f353e76558a1d2785e0fdbfb5e8df16608e'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

