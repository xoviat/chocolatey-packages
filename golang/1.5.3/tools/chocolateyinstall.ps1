
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.5.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.5.3.windows-amd64.msi'
	checksum      = '30f9fc4d8df5f327a6a31fa170e282029b92f115dcb03a92f58564ac95d5ec30'
	checksumType  = 'sha256'
	checksum64    = '5ac73c76bc3a03757091ee225299ef325b56a85798f7e4142b8f4329b24bfa44'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

