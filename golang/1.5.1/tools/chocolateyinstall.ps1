
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.5.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.5.1.windows-amd64.msi'
	checksum      = '1a56ffa99654cc76fb303f661c5487e9193d00fa5509ebba3d9d4d51fc0301b7'
	checksumType  = 'sha256'
	checksum64    = 'bd0f53739b049bcd05ec04680931279be7671f6a6edc484abaa4eff180bf3733'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

