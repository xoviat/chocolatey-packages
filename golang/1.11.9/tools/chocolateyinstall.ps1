
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.9.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.9.windows-amd64.msi'
	checksum      = 'ce7733f4693e162ed1c02ae62eb11f09ac3f9941784687b46c27e99ddf6ab7e4'
	checksumType  = 'sha256'
	checksum64    = '0c3a9836e9b21a62c59f2e21c8bc713fc8773ccfaa25f3810c03c6e68de69426'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

