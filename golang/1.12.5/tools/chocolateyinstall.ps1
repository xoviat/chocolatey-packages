
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.5.windows-amd64.msi'
	checksum      = '9e9f9685c13ef89a7e176c9996f656252219b029503d7641f531c7e079d5596e'
	checksumType  = 'sha256'
	checksum64    = '35195b53fa7f8938beb42fa7b354cca62d2521eaf187f4f84b8644942bfc50ef'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

