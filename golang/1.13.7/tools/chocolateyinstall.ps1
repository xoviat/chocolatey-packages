
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.7.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.7.windows-amd64.msi'
	checksum      = '65121ad8ce5eb2ce95bdcc09d22bd54ba5df14332af392644583275eab5d4bda'
	checksumType  = 'sha256'
	checksum64    = 'd47ee6c131ff0ac62a086e4aaef6cae380db4cb379dcbd75648ef764e99be90d'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

