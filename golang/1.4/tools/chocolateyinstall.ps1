
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.4.windows-amd64.msi'
	checksum      = 'a005500a5dcafded3e54d7206c2b40ea38d7f2e67f0bfc55a857dece7820a69e'
	checksumType  = 'sha256'
	checksum64    = '8d62e2e2cbd3b92045a51e9b750c8fa311bf6db3a8785845949febeb4a39fbe9'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

