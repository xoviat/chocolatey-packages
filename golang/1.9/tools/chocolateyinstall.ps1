
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.windows-amd64.msi'
	checksum      = '1db7a0bcdfcc78f816cf4f83df9edfc31ee9a3b3bc40ee751e193d92ee44cf31'
	checksumType  = 'sha256'
	checksum64    = '955c91d353b9a3ca2e41a1d980850141996501b3118ab09d049a2bb267103c2b'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

