
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.5.windows-amd64.msi'
	checksum      = '99134873cfdea0497eace567718e5c9a23b44e0ded29783c26145336ac8d525f'
	checksumType  = 'sha256'
	checksum64    = '01058e46f14f16d2817c762963dbd787b8326c421573bac1624cf7afbbbd499b'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

