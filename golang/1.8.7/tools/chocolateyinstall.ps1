
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.7.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.7.windows-amd64.msi'
	checksum      = '6080cb4f036836dada83acb60a2286c864d60038693dc680a80932cb200569cb'
	checksumType  = 'sha256'
	checksum64    = '3f07430020a819de55fbc2ea5b26a12d3afec204759bbcb5879b4145ed0d6359'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

