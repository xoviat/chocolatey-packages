
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.4.windows-amd64.msi'
	checksum      = 'b95cd1d1e961e823c0d3245a75f7a78180fb720c3618aff2c3f7c7364867d4fa'
	checksumType  = 'sha256'
	checksum64    = 'd967f97504e7038b4afab8014e7039e69c58ab2e7805e35889ec39ded6b70892'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

