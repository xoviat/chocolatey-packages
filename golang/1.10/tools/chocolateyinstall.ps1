
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.windows-amd64.msi'
	checksum      = 'bad7550085269fd5c9d6ef7ed0b22f2c539554802d3bf03010760f8a5b25fa15'
	checksumType  = 'sha256'
	checksum64    = '4b09094c811e0856b134628800a7820fd3e15e7f4a4fe16f0da42b5ca1a1c86c'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

