
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.6.windows-amd64.msi'
	checksum      = 'd2bee9528134e9bc73670cbc7d8c6bdb5b8d6b991f19e0037591920c6b7a353e'
	checksumType  = 'sha256'
	checksum64    = '4215d9dcc8e0bc6d2384745e6cd8d1d5347452cae9b0658e84426721c36f52e3'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

