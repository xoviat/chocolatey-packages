
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.5.windows-amd64.msi'
	checksum      = '1f13c2574a3d160402f0f90b80e298dad8b63fceaa1ce3a32d82971b6b75849d'
	checksumType  = 'sha256'
	checksum64    = '69c4be60f3431b117b3ee02a02cb18d6440dcc921960205b5b499b58a408ae1c'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

