
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.7.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.7.6.windows-amd64.msi'
	checksum      = 'f2e7a0d35c5bdc2b09b491e41103d7de624094bbda5ba9f0dae22d29edc08d90'
	checksumType  = 'sha256'
	checksum64    = 'e684807a9be72f10ed158b729e1cd188b2f96066c6c028b1da150e6971ecba6b'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

