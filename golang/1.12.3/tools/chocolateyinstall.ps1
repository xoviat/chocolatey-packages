
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.3.windows-amd64.msi'
	checksum      = 'f700a6093f85021b69269b49f4476fc865434fbd832a16724bf7927917437213'
	checksumType  = 'sha256'
	checksum64    = '699018dcb1658bbbab69adddd85cf41e3214fdde71138bcb2865823aa102a4c9'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

