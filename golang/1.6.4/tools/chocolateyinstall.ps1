
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.6.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.6.4.windows-amd64.msi'
	checksum      = '5cec63c74ba440f476ae03654d3c1bea4baadd767de192d9af9eadac5d4623b4'
	checksumType  = 'sha256'
	checksum64    = 'a0bf243f9a2fe17485bf460f3ac0b2fc66ea5b37d64de01da428f942e5108ce3'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

