
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.1.windows-amd64.msi'
	checksum      = 'c939b62e32ba3048321546a111c732868b66fe1b58ae9c12b723a02a6a02b27c'
	checksumType  = 'sha256'
	checksum64    = '49627bd99f26384452471256d6eca7860399fbcac8684a108713c321f8043486'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

