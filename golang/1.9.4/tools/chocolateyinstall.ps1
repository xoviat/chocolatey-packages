
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.4.windows-amd64.msi'
	checksum      = '5143baa5f7fba4bc4d4c4470943b192d0671de009d80d75df8744aba341bf560'
	checksumType  = 'sha256'
	checksum64    = 'eb2328ca1d15e3fd538b06b451540dec9981202083cc921201112160ae6b0020'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

