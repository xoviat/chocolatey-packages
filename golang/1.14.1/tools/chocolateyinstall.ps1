
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.14.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.14.1.windows-amd64.msi'
	checksum      = '3f305ce09b5b934e2751f362c14016f45394b2f2735760a8092ee7cd6e1676be'
	checksumType  = 'sha256'
	checksum64    = '6ea82fd995a0a3b8ff08caae86e272c2a50878f7cb2d4d19775be3a7f527b6a7'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs
