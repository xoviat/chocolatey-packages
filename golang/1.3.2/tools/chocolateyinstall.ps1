
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.3.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.3.2.windows-amd64.msi'
	checksum      = '8dd9f094c69fa23f8e77efaf4b48a80aa9c18ddff6dbe122a445a358235ddc0c'
	checksumType  = 'sha256'
	checksum64    = '487c6556bf4386133bad786cb470358003429f536e63550d167054ca18608d10'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

