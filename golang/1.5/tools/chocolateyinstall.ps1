
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.5.windows-amd64.msi'
	checksum      = '718b63f1aa14c54b001029d805c2e4addd6e0876e9df87b097f2594f3fa2e787'
	checksumType  = 'sha256'
	checksum64    = '0fb3a251fd921891ae077edf765d6dfe4fd4a2e0adf258f32e601cc10a5eddae'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

