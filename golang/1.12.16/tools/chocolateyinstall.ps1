
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.16.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.16.windows-amd64.msi'
	checksum      = '20e9ccf4476a88c7d71a38d468df165a55ca4f3e3280fdb90676a2304510284a'
	checksumType  = 'sha256'
	checksum64    = 'c7c3c5a91bb67348e2744548bbafc81049cb8c8265078b9450244abdb7030c95'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

