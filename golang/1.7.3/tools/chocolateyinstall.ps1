
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.7.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.7.3.windows-amd64.msi'
	checksum      = 'dd2ae25fc099003f5207c6125ceb4cd9444a866d4d28084a653e178514d41727'
	checksumType  = 'sha256'
	checksum64    = '1e73eb0c36af1b714389328a39de28254aca35956933ec84ca2d93175471f41a'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

