
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.4.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.4.1.windows-amd64.msi'
	checksum      = 'f577b8c75830887843c2f90cf92d01554bd2683bdc48f81a04b6a20cb06834c3'
	checksumType  = 'sha256'
	checksum64    = '36a04a8aca8b54cbe1287d1f433c11b7f16650d3b64623b6eb1ab2afd7d06dc6'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

