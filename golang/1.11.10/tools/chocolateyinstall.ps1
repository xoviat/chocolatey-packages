
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.10.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.10.windows-amd64.msi'
	checksum      = '7b218548ec55eeaa04f81029267553fb96db437c62a23dfb72b615df8977db0c'
	checksumType  = 'sha256'
	checksum64    = 'e5089960353d9d662dbb18840cd2eee8c9acef8479de646c39d65b80fe86a766'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

