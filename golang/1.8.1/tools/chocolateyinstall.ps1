
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.1.windows-amd64.msi'
	checksum      = '1bd07af205d723e33dbcc45c1ce59fc78c4f6463dd3d4d65d7834de1f8fe909e'
	checksumType  = 'sha256'
	checksum64    = 'e99c4086e7b8a62cc0d9dcedb7fdd70a36733b53e2cb67b55ae7a1bc76f4036d'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

