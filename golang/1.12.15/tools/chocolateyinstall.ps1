
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.15.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.15.windows-amd64.msi'
	checksum      = 'a5f03770805a304e8ed1f959372456b821e905d147de3df778d82da940d3559a'
	checksumType  = 'sha256'
	checksum64    = '56728500971719a18f757d03f133f9abf71738ba47ccf220d8ddc20222e9eaab'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

