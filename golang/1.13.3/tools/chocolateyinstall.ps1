
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.3.windows-amd64.msi'
	checksum      = '24504c97193e0bac76eab3273b77afbfd545791b7397c373f4cbfe89d808cc13'
	checksumType  = 'sha256'
	checksum64    = 'd6d1a3287c994574f88a8650a1e7f4fff19babc04979c9a8cf0178ef9a49ed10'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

