
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.8.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.8.windows-amd64.msi'
	checksum      = 'ae6fa7aa90e73dcf6dbf20a129682cb9665286f1aa423f4d985b3e03eed8f1b2'
	checksumType  = 'sha256'
	checksum64    = '4d35f1e91285711ee92597f6ee3bd066fda2fa5157965c62d8f662a6ce43056a'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

