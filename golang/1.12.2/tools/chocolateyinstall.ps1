
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.2.windows-amd64.msi'
	checksum      = '66090b5fb67e9f1dc59c14afb65757b2ad2cd8617b9b3f8dcc1d5fc47fb40841'
	checksumType  = 'sha256'
	checksum64    = '5c88452cefe612c8eca20e5c1ff4a656256eccd80078d87d7b96d090f9ee7cef'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

