
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.4.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.4.3.windows-amd64.msi'
	checksum      = 'ae55f5af5ef823d641ff87a80485bd8eea653f59c851fdb61ff6defa72d11bad'
	checksumType  = 'sha256'
	checksum64    = '882b9d708bab9f6b594a5b0741c39252bc6d366fa60b0650732e0cf00870fc15'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

