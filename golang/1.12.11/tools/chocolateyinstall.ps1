
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.11.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.11.windows-amd64.msi'
	checksum      = 'b009b2acb511cf55526c335afc8001227082f709c13e30568f5388c262915dea'
	checksumType  = 'sha256'
	checksum64    = 'ea0d25fad7c2c2d1db2c2f27f088e2ff5e552b9d3b91bfc5788445f0da4f13d5'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

