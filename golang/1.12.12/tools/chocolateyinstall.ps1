
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.12.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.12.windows-amd64.msi'
	checksum      = '980b7f28ed02d1481f1e8cd095d61c5a137754dcd0bf62c3fec9dd3b5a2743b8'
	checksumType  = 'sha256'
	checksum64    = '58dca433f4da832cb672c3bfc0cecb0e1ab93107607f92500c2265dee4eda4d2'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

