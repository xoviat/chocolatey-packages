
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.7.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.7.windows-amd64.msi'
	checksum      = '8fe71ebf78ab68ef7466c8496312a0c6f8af5f9ec8b186b02eb5f3bdfabd5b17'
	checksumType  = 'sha256'
	checksum64    = '795cc0353deb5f7bd54eff95444b9124d803b0adf28636c64d914bffee14b81c'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

