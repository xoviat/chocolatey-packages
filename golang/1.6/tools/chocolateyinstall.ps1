
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.6.windows-amd64.msi'
	checksum      = 'be2f9e1c85bfc55b3bea8f1e48acf4a8117fbcd6c7f372aa9ff9f74429f18a35'
	checksumType  = 'sha256'
	checksum64    = '9e185fe7985505e3a65633f5e4db76664607f67f8331f0ce4986ba69b51015b7'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

