
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.5.windows-amd64.msi'
	checksum      = '7d379ad31466584ec2745090bb5e130afd88c8c97a5d3fdbde29dbad0e792abe'
	checksumType  = 'sha256'
	checksum64    = '9119f7fc40037a47254e139a410c2ecf5372baa6bb830a105d7e4d6df5b4b9a7'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

