
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.2.windows-amd64.msi'
	checksum      = '56dc82a16747be3b94213cd53a059437462437bc67a087552111324d3f64877a'
	checksumType  = 'sha256'
	checksum64    = '74e0221315bb79c45080cd0c81bbe046ab2a21bea808fa5e2d119f3a07815218'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

