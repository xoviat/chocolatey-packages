
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.4.windows-amd64.msi'
	checksum      = '2686fdeee3ddc8321f50fe0740728092f98732fdc2db6521b13816adcc8dff8a'
	checksumType  = 'sha256'
	checksum64    = 'd58581d48a9eccdb4f8931efd6c7037d34598625a6ecdedf2012da1435dc5770'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

