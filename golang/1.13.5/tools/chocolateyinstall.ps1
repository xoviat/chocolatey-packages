
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.5.windows-amd64.msi'
	checksum      = 'bfbc8a355db120c010a5af573438939762480d31f2c9f76d2080ee1cc338fef0'
	checksumType  = 'sha256'
	checksum64    = 'eabcf66d6d8f44de21a96a18d957990c62f21d7fadcb308a25c6b58c79ac2b96'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

