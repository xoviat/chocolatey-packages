
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.3.windows-amd64.msi'
	checksum      = 'f617ee108a0e1fed515ba6f661bc3e324847ae57c93e75cf751f7a3e26c7dc29'
	checksumType  = 'sha256'
	checksum64    = 'c1eb61762d5ec585b8af60ff7fa7b65fbb35a51771bf2b97b961b13058832d8b'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

