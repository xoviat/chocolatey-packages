
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.7.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.7.4.windows-amd64.msi'
	checksum      = '7a465629d7122f1c00f59b25381f2d7efc245f1ad6cee0aa5fe6a5b54adc759a'
	checksumType  = 'sha256'
	checksum64    = '1d8b5a79f0d73b4a5b78f701eef8526ee212b3c54486498a2016fc83958e3c50'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

