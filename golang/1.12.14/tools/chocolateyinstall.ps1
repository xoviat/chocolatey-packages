
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.14.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.14.windows-amd64.msi'
	checksum      = 'b9d3311dcde3dc462d4fe6ea5a0c5a548153a53f5b3d14401a00cd8029bcef15'
	checksumType  = 'sha256'
	checksum64    = '76bf6d1c0505be1b4e7ef682a160675787b997f6e60837fc63ff8279dafe050f'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

