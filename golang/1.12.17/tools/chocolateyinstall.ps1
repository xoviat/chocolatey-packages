
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.17.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.17.windows-amd64.msi'
	checksum      = '95956cf2ea2eb364cd78d91f2b7e042e93fab202dca2d74254c4475a0bf9a4d0'
	checksumType  = 'sha256'
	checksum64    = '803033c946b3333be4e0a26e854806d914594becc3156eff6acf7dcd2e61e25c'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

