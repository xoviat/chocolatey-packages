
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.4.windows-amd64.msi'
	checksum      = '79d22dfd70eac5231cbb6ff22cdb699bc520fc6f1e6cbbaf000c687b9fd81cbf'
	checksumType  = 'sha256'
	checksum64    = '4db2ce3c570854dd047aff38a978d9271049e901bfd4703c6be72eae3d91296c'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

