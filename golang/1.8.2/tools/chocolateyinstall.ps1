
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.2.windows-amd64.msi'
	checksum      = 'e0f57dc6ef1df0952c3f5fb343cda4a336212ee3fbce93fd124e062c7e7049ac'
	checksumType  = 'sha256'
	checksum64    = '710c5dcbfcb5a71dfdb24d4536617cb4d0d7786488f45ca1602bf2648edbff0a'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

