
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.5.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.5.windows-amd64.msi'
	checksum      = 'af262a08af78c4f92a0b735fd45d4422ee3cbbd13b0083bb257e6d34fd2c0ca8'
	checksumType  = 'sha256'
	checksum64    = 'fed8db8753a45de28f7cac8d7d05196d02dde3b244c3dfda9beb28d2eea6c27a'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

