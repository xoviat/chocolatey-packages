
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.13.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.13.windows-amd64.msi'
	checksum      = '5695e93ca2a23837a499d78286fb267f74c3e9f30aaf928c135b5a7321adf18b'
	checksumType  = 'sha256'
	checksum64    = '21b2afb755d97ec4533a08f9e926911bff23866cce707f9b21d0c680b6a303c4'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

