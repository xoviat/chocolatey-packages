
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.4.windows-amd64.msi'
	checksum      = 'f83f2c4774d7e90721962833e47804bb5d47a35c0c5e3a416df9986ddf786176'
	checksumType  = 'sha256'
	checksum64    = '39a062ffa5bed0ba99b7ca8422c4c362e3b904461535ade77a8c4fa37ebd1347'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

