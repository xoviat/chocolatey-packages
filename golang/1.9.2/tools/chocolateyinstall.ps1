
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.2.windows-amd64.msi'
	checksum      = '020ea4a53093dd98b5ad074c4e493ff52be0aa71eee89dc24ca7783cb528de97'
	checksumType  = 'sha256'
	checksum64    = 'daeb761aa6fdb22dc3954fd911963b347c44aa5c6ba974b9c01be7cbbd6922ba'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

