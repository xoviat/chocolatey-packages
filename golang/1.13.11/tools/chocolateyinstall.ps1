
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.11.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.11.windows-amd64.msi'
	checksum      = '47665a24b7ca82cffda51b8aa72ce304a88b36056019e6eff09fa84cfe09310b'
	checksumType  = 'sha256'
	checksum64    = 'e40402bf077c8b9c82bdfd33277565555416f8df3c5034ea3ccff34ca917b499'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs
