
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.9.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.9.windows-amd64.msi'
	checksum      = 'ba5b57d3752d45953e8f616f2371f65ef4c953cf8c6e2e0e64d6a1496df3ab08'
	checksumType  = 'sha256'
	checksum64    = 'da2fc362c7afcab611709ef56e1bb145c92dfaf361cafdd75634ac8172ded245'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

