
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.14.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.14.windows-amd64.msi'
	checksum      = 'e38482d6ba1b421fc1a3d7190fb042ba07f79ca0d186dd3105e28fe92ef20f07'
	checksumType  = 'sha256'
	checksum64    = '5d3b87736bf9e86e971055e5db61e8b4ec31405f42d954ad3a71ac758876685e'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

