
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.9.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.9.windows-amd64.msi'
	checksum      = '552d46552deba51b242016dabf69b9c4ca995e5d2a9c609fbdc6ce987947d9e9'
	checksumType  = 'sha256'
	checksum64    = '720543a0e507cc3a6924ee91cdc40e23e7ade8741c0a159871239705063b1026'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs
