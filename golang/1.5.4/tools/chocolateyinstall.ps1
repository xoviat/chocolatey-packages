
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.5.4.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.5.4.windows-amd64.msi'
	checksum      = '716054681f6a4c64e23b81b4399f29f7e6f10fd543fe9d66e420e933b5a96b64'
	checksumType  = 'sha256'
	checksum64    = 'd3023d462d6391aff4e75350cbf0abbe44b3eac26213413c08aa30f87421d393'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

