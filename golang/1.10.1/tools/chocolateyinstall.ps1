
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.1.windows-amd64.msi'
	checksum      = '212e08ea4857e9fc308be2cb6cf3488ab26c83de36e46f4fefabf2296d8f9d48'
	checksumType  = 'sha256'
	checksum64    = 'b8cd4eb0c9ce2b7e50a2c69fb21bf8075b60fecc77c5caa1a60bdcb805f71803'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

