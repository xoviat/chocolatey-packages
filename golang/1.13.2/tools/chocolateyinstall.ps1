
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.2.windows-amd64.msi'
	checksum      = 'c3963ea238088741c28b41fcf8b5c21a82d100b033912984c5a0b5c2ce3f936f'
	checksumType  = 'sha256'
	checksum64    = '33395a6939deead15fe8ec85dd217a48d38cbd809e9cd9bc63a41bfcfad3b942'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

