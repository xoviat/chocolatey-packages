
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.2.windows-amd64.msi'
	checksum      = 'd793ab6d8a79ec40c1994ef497caead917c9e1d3bbd8b89ef730639ea17470cb'
	checksumType  = 'sha256'
	checksum64    = 'fcbb828ec6fccd05cf51fc1b7815c78b315f23cd5aaf6a37395d8c8ba64f6b0b'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

