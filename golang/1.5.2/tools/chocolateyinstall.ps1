
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.5.2.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.5.2.windows-amd64.msi'
	checksum      = '109a23e7e22f3491f79e9a0fc47665deb10f2a460833a23cb8a65aa398444332'
	checksumType  = 'sha256'
	checksum64    = 'b2b284d190bceaf723c6368972bbb4b627d928f71857f032f8c4943305c22a35'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

