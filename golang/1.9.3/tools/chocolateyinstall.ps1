
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.3.windows-amd64.msi'
	checksum      = '946712c42459bf22f49bc9fa46223a7322285b0965d619c7a626c1c1b78b7189'
	checksumType  = 'sha256'
	checksum64    = '3d178a5f9dd10f24906cbc9d85d20489125fdb098967c967137f42df92509273'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

