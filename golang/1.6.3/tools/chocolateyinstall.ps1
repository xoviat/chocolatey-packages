
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.6.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.6.3.windows-amd64.msi'
	checksum      = '7838ded5a07c316fdd2f2f5613b68d0b05b7f3094e024f8c8336f789e1960525'
	checksumType  = 'sha256'
	checksum64    = 'f71e929dd0846f12d9b683cb2896e9b651a33c69bdbab9f62e9cc001dcc61798'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

