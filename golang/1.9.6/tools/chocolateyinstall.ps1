
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.6.windows-amd64.msi'
	checksum      = '6377df9f010633cef03a99f0e4eee2fe886b8305f07540b67e4ca1f1fb6d9ef7'
	checksumType  = 'sha256'
	checksum64    = 'd0d8942f75528adc1473f38cce423ab139ddad892a523fee9f43315601fd4caa'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

