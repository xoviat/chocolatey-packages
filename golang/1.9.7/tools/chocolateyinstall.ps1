
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.9.7.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.9.7.windows-amd64.msi'
	checksum      = '877c9484adb2d0acf858aa3de19580c5b54e009282c7ea78123e358be5ffc244'
	checksumType  = 'sha256'
	checksum64    = '12958344cc7279809ca77badf5d18f8a19685d956cba49bd33a67728b4730d10'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

