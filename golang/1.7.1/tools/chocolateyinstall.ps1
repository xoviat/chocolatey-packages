
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.7.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.7.1.windows-amd64.msi'
	checksum      = 'ffb26e5b080e6fead2a1973af34e44b22884fcdd9c61f01e967844cb747d006f'
	checksumType  = 'sha256'
	checksum64    = 'c043eebd653e84fac259f725621e600ffed80d45bea62e35855fd21dfc7075dc'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

