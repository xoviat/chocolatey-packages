
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.3.3.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.3.3.windows-amd64.msi'
	checksum      = 'fe99da9755d4415a597e1e96e276731dfd1855c55d2db256b4fe5bf64577f793'
	checksumType  = 'sha256'
	checksum64    = '0ad026ebd99e47d0bbae5adf36e82c1dc8977ce3f05df815a1d48da46f87302f'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

