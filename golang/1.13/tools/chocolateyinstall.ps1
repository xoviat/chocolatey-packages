
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.windows-amd64.msi'
	checksum      = '203e21e36eccd41c9ec840f5bd917faffa9213dbc417a10effefda3aebe2bcd5'
	checksumType  = 'sha256'
	checksum64    = '548ae0fc6b07dacf90cbf50f1ca3cc7fd25af53be67542befd36ee117b669e12'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

