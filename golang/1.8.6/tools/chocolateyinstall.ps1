
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.6.windows-amd64.msi'
	checksum      = '9dc3c3adf5c829104c27efcd946de7ba043902b708384145e6ae59fef909a960'
	checksumType  = 'sha256'
	checksum64    = '8f8166e01a941adfebd1d734397c25da86358926008d007382c0857c7bfbb559'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

