
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.13.1.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.13.1.windows-amd64.msi'
	checksum      = 'c4ca4ea88e8cebafcf16da666a79007aae1f431010671a0c239fed2a2286a63b'
	checksumType  = 'sha256'
	checksum64    = 'ee6ff4f31766096e6ff00bdd855e007c473cc9080f3588d7be0c5ec0e0b85578'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

