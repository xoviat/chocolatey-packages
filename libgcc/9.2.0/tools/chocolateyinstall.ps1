
$pp = Get-PackageParameters
if (!$pp.InstallDir) { $pp.InstallDir = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install" }
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $pp.InstallDir
	fileType      = 'zip'
	url           = 'https://osdn.net/projects/mingw/downloads/72215/libgcc-9.2.0-3-mingw32-dll-1.tar.xz/'
	url64bit      = 'https://osdn.net/projects/mingw/downloads/72215/libgcc-9.2.0-3-mingw32-dll-1.tar.xz/'
	checksum      = '27f7a72e1aa5d0bb894f84aa62da0200b3a4a334e85457bb1961fc341290f833'
	checksumType  = 'sha256'
	checksum64    = '27f7a72e1aa5d0bb894f84aa62da0200b3a4a334e85457bb1961fc341290f833'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $pp.InstallDir | Out-Null
Install-ChocolateyZipPackage @packageArgs

$install = $(Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install")

$tar = $(Get-ChildItem -r $install | Where-Object { $_.Name -match '\.tar$' }).FullName
Get-ChocolateyUnzip -FileFullPath $tar -Destination $install

Install-ChocolateyPath $(Join-Path $install "bin")

