
$install = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $install
	fileType      = 'zip'
	url           = 'https://github.com/niXman/mingw-builds-binaries/releases/download/13.1.0-rt_v11-rev1/i686-13.1.0-release-posix-dwarf-ucrt-rt_v11-rev1.7z'
	url64bit      = 'https://github.com/niXman/mingw-builds-binaries/releases/download/13.1.0-rt_v11-rev1/x86_64-13.1.0-release-posix-seh-ucrt-rt_v11-rev1.7z'
	checksum      = '63c4ada31dca519cd1894e19381afaed75d88d36e5891be94268b07f95171d0c'
	checksumType  = 'sha256'
	checksum64    = 'da69a72f8f7e021a738b5beb45c08398cc042cca8f8573d0f2ae263e65f5752a'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $install | Out-Null
Install-ChocolateyZipPackage @packageArgs

Move-Item $install "C:\ProgramData\mingw64"
$install = "C:\ProgramData\mingw64"

$("mingw32", "mingw64") | ForEach {
  $bin = (Join-Path $install (Join-Path $_ "bin"))
  Write-Output "Testing path: $bin"
  If (Test-Path $bin) {
    Install-ChocolateyPath $bin
  }
}

