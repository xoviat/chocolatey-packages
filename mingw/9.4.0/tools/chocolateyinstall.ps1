
$pp = Get-PackageParameters
if (!$pp.InstallDir) { $pp.InstallDir = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install" }
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $pp.InstallDir
	fileType      = 'zip'
	url           = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/9.4.0-9.0.0-r1/winlibs-i686-posix-dwarf-gcc-9.4.0-mingw-w64-9.0.0-r1.7z'
	url64bit      = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/9.4.0-9.0.0-r1/winlibs-x86_64-posix-seh-gcc-9.4.0-mingw-w64-9.0.0-r1.7z'
	checksum      = '1b3d3c44dd21d6b31d64bdb8175a3c8081dbe1e3bf9c3dd46ac441cc000da73a'
	checksumType  = 'sha256'
	checksum64    = 'a8f9a0d345e5f57619e67692b50cb56bb3e0f45d6478531f056e9ce4c822218f'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $pp.InstallDir | Out-Null
Install-ChocolateyZipPackage @packageArgs

$("mingw32", "mingw64") | ForEach {
  $bin = (Join-Path "$pp.InstallDir" (Join-Path $_ "bin"))
  If (Test-Path $bin) {
    Install-ChocolateyPath $bin
  }
}

