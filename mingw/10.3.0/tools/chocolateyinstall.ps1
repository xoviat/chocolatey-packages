
$pp = Get-PackageParameters
if (!$pp.InstallDir) { $pp.InstallDir = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install" }
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $pp.InstallDir
	fileType      = 'zip'
	url           = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/10.3.0-12.0.0-9.0.0-r2/winlibs-i686-posix-dwarf-gcc-10.3.0-mingw-w64-9.0.0-r2.7z'
	url64bit      = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/10.3.0-12.0.0-9.0.0-r2/winlibs-x86_64-posix-seh-gcc-10.3.0-mingw-w64-9.0.0-r2.7z'
	checksum      = '94aa4398bc84f0b9ce79f65f9066621822376df2835b10e28ae8164239023064'
	checksumType  = 'sha256'
	checksum64    = '0455647fc918c7fab9612dcbfde4cc93a3ecf47c5b5c251e13fabaab31f5c6e9'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $pp.InstallDir | Out-Null
Install-ChocolateyZipPackage @packageArgs

$("mingw32", "mingw64") | ForEach {
  $bin = (Join-Path "$pp.InstallDir" (Join-Path $_ "bin"))
  If (Test-Path $bin) {
    Install-ChocolateyPath $bin
  }
}

