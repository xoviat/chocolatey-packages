
$pp = Get-PackageParameters
if (!$pp.InstallDir) { $pp.InstallDir = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install" }
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $pp.InstallDir
	fileType      = 'zip'
	url           = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/11.1.0-12.0.0-9.0.0-r3/winlibs-i686-posix-dwarf-gcc-11.1.0-mingw-w64-9.0.0-r3.7z'
	url64bit      = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/11.1.0-12.0.0-9.0.0-r3/winlibs-x86_64-posix-seh-gcc-11.1.0-mingw-w64-9.0.0-r3.7z'
	checksum      = '8b95bafc945ae99d57c17958fcbc14a57960177cb5a99e077defe0319c5bb89b'
	checksumType  = 'sha256'
	checksum64    = 'eedc0481e8cd4a4a82ffc91baec4412347d0bd4baaf546315be7a9b113316843'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $pp.InstallDir | Out-Null
Install-ChocolateyZipPackage @packageArgs

$("mingw32", "mingw64") | ForEach {
  $bin = (Join-Path "$pp.InstallDir" (Join-Path $_ "bin"))
  If (Test-Path $bin) {
    Install-ChocolateyPath $bin
  }
}

