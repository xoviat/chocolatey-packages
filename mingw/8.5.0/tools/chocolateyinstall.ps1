
$pp = Get-PackageParameters
if (!$pp.InstallDir) { $pp.InstallDir = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install" }
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $pp.InstallDir
	fileType      = 'zip'
	url           = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/8.5.0-9.0.0-r1/winlibs-i686-posix-dwarf-gcc-8.5.0-mingw-w64-9.0.0-r1.7z'
	url64bit      = 'https://github.com/brechtsanders/winlibs_mingw/releases/download/8.5.0-9.0.0-r1/winlibs-x86_64-posix-seh-gcc-8.5.0-mingw-w64-9.0.0-r1.7z'
	checksum      = 'ad6a2fd062faf73363a49a623412e635e5351f8c7f1a59e47071ba0dc8108db5'
	checksumType  = 'sha256'
	checksum64    = '6cff16ec42e717f3d0076378eb4f93fe894bc61ba6339deadc7113ef63f40e08'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $pp.InstallDir | Out-Null
Install-ChocolateyZipPackage @packageArgs

$("mingw32", "mingw64") | ForEach {
  $bin = (Join-Path "$pp.InstallDir" (Join-Path $_ "bin"))
  If (Test-Path $bin) {
    Install-ChocolateyPath $bin
  }
}

