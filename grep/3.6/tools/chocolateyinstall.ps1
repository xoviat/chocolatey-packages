

$install = $(Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install")

$file64 = $(Get-ChildItem -r $install | Where-Object { $_.Name -match '^grep-[0-9\.]+-x64\.exe$' }).FullName
$target = Join-Path $install "grep.exe"
$file = $file64

Copy-Item $file $target
Remove-Item $file64

"@echo off `ngrep.exe -E %*" | Out-File $(Join-Path $install egrep.bat) -encoding ASCII
"@echo off `ngrep.exe -F %*" | Out-File $(Join-Path $install fgrep.bat) -encoding ASCII

Install-Binfile -Name egrep -Path $(Join-Path $install "egrep.bat")
Install-Binfile -Name fgrep -Path $(Join-Path $install "fgrep.bat")
