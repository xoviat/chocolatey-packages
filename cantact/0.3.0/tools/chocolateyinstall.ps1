

$install = $(Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install")

$file32 = $(Get-ChildItem -r $install | Where-Object { $_.Name -match '^cantact\.exe$' }).FullName
$file64 = $(Get-ChildItem -r $install | Where-Object { $_.Name -match '^cantact64\.exe$' }).FullName
$target = Join-Path $install "cantact.exe"

if (((Get-ProcessorBits 32) -or $env:ChocolateyForceX86 -eq $true) -or ($file64 -eq $null -or $file64 -eq '')) {
  $file = $file32
} else {
  $file = $file64
}

Copy-Item $file $target
Remove-Item $file32, $file64
