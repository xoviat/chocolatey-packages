# Chocolatey packages repository

NOTICE:

This repository exists soley to comply with chocolatey moderator policy.
The entire repository is automatically generated from a program that I have
written called chauto. Chauto scrapes the web, using a configuration file, to
identify and reformat new versions of software to comply with chocolatey
moderator policy. Because package generation is entirely automated, changes to
this repository will not be accepted.
