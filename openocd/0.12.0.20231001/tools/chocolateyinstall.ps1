
$install = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $install
	fileType      = 'zip'
	url           = 'https://github.com/openocd-org/openocd/releases/download/v0.12.0/openocd-v0.12.0-i686-w64-mingw32.tar.gz'
	url64bit      = 'https://github.com/openocd-org/openocd/releases/download/v0.12.0/openocd-v0.12.0-i686-w64-mingw32.tar.gz'
	checksum      = 'd7168545a6d5df4772b6090d470650f3eb8c9732dbd19b1f9027824c7f4a6fa3'
	checksumType  = 'sha256'
	checksum64    = 'd7168545a6d5df4772b6090d470650f3eb8c9732dbd19b1f9027824c7f4a6fa3'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $install | Out-Null
Install-ChocolateyZipPackage @packageArgs

$tar = $(Get-ChildItem -r $install | Where-Object { $_.Name -match '^.*\.tar$' }).FullName
echo "Found tar: $tar"
Get-ChocolateyUnzip -FileFullPath $tar -Destination $install

