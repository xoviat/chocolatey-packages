
$install = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $install
	fileType      = 'zip'
	url           = 'https://github.com/openocd-org/openocd/releases/download/v0.11.0/openocd-v0.11.0-i686-w64-mingw32.tar.gz'
	url64bit      = 'https://github.com/openocd-org/openocd/releases/download/v0.11.0/openocd-v0.11.0-i686-w64-mingw32.tar.gz'
	checksum      = 'b195e3c3e9fe3f4fe9dfd9780ff08336ef8ed3015a76cc883d9e21e57c456f33'
	checksumType  = 'sha256'
	checksum64    = 'b195e3c3e9fe3f4fe9dfd9780ff08336ef8ed3015a76cc883d9e21e57c456f33'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $install | Out-Null
Install-ChocolateyZipPackage @packageArgs

