INPLACE(3am)               GNU Awk Extension Modules              INPLACE(3am)



NNAAMMEE
       inplace - emulate sed/perl/ruby in-place editing

SSYYNNOOPPSSIISS
       gawk -i inplace ...

DDEESSCCRRIIPPTTIIOONN
       The  _i_n_p_l_a_c_e extension adds two functions named iinnppllaaccee__bbeeggiinn(()) and iinn--
       ppllaaccee__eenndd(()).  These functions are meant to  be  invoked  from  the  _i_n_-
       _p_l_a_c_e_._a_w_k wrapper which is installed when _g_a_w_k is.

       By  default, each named file on the command line is replaced with a new
       file of the same name whose contents are the results of running the AWK
       program.  If the user supplies an AWK variable named iinnppllaaccee::::ssuuffffiixx in
       a BBEEGGIINN rule or on the command line, then the  _i_n_p_l_a_c_e  extension  con-
       catenates that suffix onto the original filename and uses the result as
       a filename for renaming the original.

       For backwards compatibility, the variable will also check  IINNPPLLAACCEE__SSUUFF--
       FFIIXX  (in the aawwkk namespace) for the suffix to use if iinnppllaaccee::::ssuuffffiixx is
       not set.

       One can disable inplace editing  selectively  by  placing  iinnppllaaccee::::eenn--
       aabbllee==00 on the command line prior to files that should be processed nor-
       mally.  One can reenable inplace editing by  placing  iinnppllaaccee::::eennaabbllee==11
       prior to files that should be subject to inplace editing.

BBUUGGSS
       While the extension does attempt to preserve ownership and permissions,
       it makes no attempt to copy the ACLs from the original file.

       If the program dies prematurely, as might happen if an unhandled signal
       is received, a temporary file may be left behind.

EEXXAAMMPPLLEE
       gawk -i inplace '_s_c_r_i_p_t' files ...
       gawk -i inplace -f _s_c_r_i_p_t_f_i_l_e files ...

SSEEEE AALLSSOO
       _G_A_W_K_:   _E_f_f_e_c_t_i_v_e   _A_W_K   _P_r_o_g_r_a_m_m_i_n_g,   _f_i_l_e_f_u_n_c_s(3am),  _f_n_m_a_t_c_h(3am),
       _f_o_r_k(3am), _o_r_d_c_h_r(3am),  _r_e_a_d_d_i_r(3am),  _r_e_a_d_f_i_l_e(3am),  _r_e_v_o_u_t_p_u_t(3am),
       _r_w_a_r_r_a_y(3am).

AAUUTTHHOORR
       Andrew Schorr, sscchhoorrrr@@tteelleemmeettrryy--iinnvveessttmmeennttss..ccoomm.

CCOOPPYYIINNGG PPEERRMMIISSSSIIOONNSS
       Copyright  (C)  2012, 2013, 2015, 2018, 2019, Free Software Foundation,
       Inc.

       Permission is granted to make and distribute verbatim  copies  of  this
       manual  page  provided  the copyright notice and this permission notice
       are preserved on all copies.

       Permission is granted to copy and distribute modified versions of  this
       manual  page  under  the conditions for verbatim copying, provided that
       the entire resulting derived work is distributed under the terms  of  a
       permission notice identical to this one.

       Permission  is granted to copy and distribute translations of this man-
       ual page into another language, under the above conditions for modified
       versions,  except that this permission notice may be stated in a trans-
       lation approved by the Foundation.



Free Software Foundation          Jun 26 2018                     INPLACE(3am)
