FNMATCH(3am)               GNU Awk Extension Modules              FNMATCH(3am)



NNAAMMEE
       fnmatch - compare a string against a filename wildcard

SSYYNNOOPPSSIISS
       @load "fnmatch"

       result = fnmatch(pattern, string, flags)

DDEESSCCRRIIPPTTIIOONN
       The  _f_n_m_a_t_c_h extension provides an AWK interface to the _f_n_m_a_t_c_h(3) rou-
       tine.  It adds a single function named ffnnmmaattcchh(()), one predefined  vari-
       able (FFNNMM__NNOOMMAATTCCHH), and an array of flag values named FFNNMM.

       The first argument is the filename wildcard to match, the second is the
       filename string, and the third is either zero, or the bitwise OR of one
       or more of the flags in the FFNNMM array.

       The  return value is zero on success, FFNNMM__NNOOMMAATTCCHH if the string did not
       match the pattern, or a different non-zero value if an error occurred.

       The flags are follows:

       FFNNMM[[""CCAASSEEFFOOLLDD""]]
              Corresponds to the FFNNMM__CCAASSEEFFOOLLDD flag as defined in _f_n_m_a_t_c_h(3).

       FFNNMM[[""FFIILLEE__NNAAMMEE""]]
              Corresponds to the FFNNMM__FFIILLEE__NNAAMMEE flag as defined in _f_n_m_a_t_c_h(3).

       FFNNMM[[""LLEEAADDIINNGG__DDIIRR""]]
              Corresponds to  the  FFNNMM__LLEEAADDIINNGG__DDIIRR  flag  as  defined  in  _f_n_-
              _m_a_t_c_h(3).

       FFNNMM[[""NNOOEESSCCAAPPEE""]]
              Corresponds to the FFNNMM__NNOOEESSCCAAPPEE flag as defined in _f_n_m_a_t_c_h(3).

       FFNNMM[[""PPAATTHHNNAAMMEE""]]
              Corresponds to the FFNNMM__PPAATTHHNNAAMMEE flag as defined in _f_n_m_a_t_c_h(3).

       FFNNMM[[""PPEERRIIOODD""]]
              Corresponds to the FFNNMM__PPEERRIIOODD flag as defined in _f_n_m_a_t_c_h(3).

NNOOTTEESS
       Nothing prevents AWK code from changing the predefined variable FFNNMM__NNOO--
       MMAATTCCHH, but doing so may cause strange results.

EEXXAAMMPPLLEE
       @load "fnmatch"
       ...
       flags = or(FNM["PERIOD"], FNM["NOESCAPE"])
       if (fnmatch("*.a", "foo.c", flags) == FNM_NOMATCH)
            print "no match"

SSEEEE AALLSSOO
       _G_A_W_K_:  _E_f_f_e_c_t_i_v_e  _A_W_K  _P_r_o_g_r_a_m_m_i_n_g,  _f_i_l_e_f_u_n_c_s(3am),   _f_o_r_k(3am),   _i_n_-
       _p_l_a_c_e(3am),  _o_r_d_c_h_r(3am),  _r_e_a_d_d_i_r(3am), _r_e_a_d_f_i_l_e(3am), _r_e_v_o_u_t_p_u_t(3am),
       _r_w_a_r_r_a_y(3am), _t_i_m_e(3am).

       _f_n_m_a_t_c_h(3).

AAUUTTHHOORR
       Arnold Robbins, aarrnnoolldd@@sskkeeeevvee..ccoomm.

CCOOPPYYIINNGG PPEERRMMIISSSSIIOONNSS
       Copyright (C) 2012, 2013, 2018, Free Software Foundation, Inc.

       Permission is granted to make and distribute verbatim  copies  of  this
       manual  page  provided  the copyright notice and this permission notice
       are preserved on all copies.

       Permission is granted to copy and distribute modified versions of  this
       manual  page  under  the conditions for verbatim copying, provided that
       the entire resulting derived work is distributed under the terms  of  a
       permission notice identical to this one.

       Permission  is granted to copy and distribute translations of this man-
       ual page into another language, under the above conditions for modified
       versions,  except that this permission notice may be stated in a trans-
       lation approved by the Foundation.



Free Software Foundation          Feb 21 2018                     FNMATCH(3am)
