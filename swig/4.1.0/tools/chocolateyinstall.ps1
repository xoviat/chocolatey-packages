
$pp = Get-PackageParameters
if (!$pp.InstallDir) { $pp.InstallDir = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install" }
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $pp.InstallDir
	fileType      = 'zip'
	url           = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-4.1.0/swigwin-4.1.0.zip/download'
	url64bit      = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-4.1.0/swigwin-4.1.0.zip/download'
	checksum      = '4c3c59bb86ba628683d6253b011291af769a2bd83b3d996db36a021b15e3b179'
	checksumType  = 'sha256'
	checksum64    = '4c3c59bb86ba628683d6253b011291af769a2bd83b3d996db36a021b15e3b179'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $pp.InstallDir | Out-Null
Install-ChocolateyZipPackage @packageArgs

