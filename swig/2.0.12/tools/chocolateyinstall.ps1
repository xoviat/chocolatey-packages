
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'zip'
	url           = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-2.0.12/swigwin-2.0.12.zip/download'
	url64bit      = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-2.0.12/swigwin-2.0.12.zip/download'
	checksum      = 'ac7b879e686aa4b142eb85f9f4d16395d879486888aaf5e1078da42e685f0270'
	checksumType  = 'sha256'
	checksum64    = 'ac7b879e686aa4b142eb85f9f4d16395d879486888aaf5e1078da42e685f0270'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyZipPackage @packageArgs

