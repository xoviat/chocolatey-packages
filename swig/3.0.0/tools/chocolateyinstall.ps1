
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'zip'
	url           = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-3.0.0/swigwin-3.0.0.zip/download'
	url64bit      = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-3.0.0/swigwin-3.0.0.zip/download'
	checksum      = 'be829be4b87cc6a24d88cd9771a8c3c2eabcc72095f4d370d01a0080aae1a060'
	checksumType  = 'sha256'
	checksum64    = 'be829be4b87cc6a24d88cd9771a8c3c2eabcc72095f4d370d01a0080aae1a060'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyZipPackage @packageArgs

