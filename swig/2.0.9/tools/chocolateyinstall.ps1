
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'zip'
	url           = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-2.0.9/swigwin-2.0.9.zip/download'
	url64bit      = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-2.0.9/swigwin-2.0.9.zip/download'
	checksum      = '376a868f0d8b2e43315260750931c004a56bede2dbd6452a1433fc7fc383a6ff'
	checksumType  = 'sha256'
	checksum64    = '376a868f0d8b2e43315260750931c004a56bede2dbd6452a1433fc7fc383a6ff'
	checksumType64= 'sha256'

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyZipPackage @packageArgs

